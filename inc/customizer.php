<?php
/**
 * SJU Wordpress Theme Theme Customizer.
 *
 * @package SJU_Wordpress_Theme
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function sju_theme_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	
	
	$wp_customize->add_setting(
    'header_bg_color',
    array(
      'default'     => '#9e1b32'
    )
  );
  $wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'header_bg_color',
        array(
            'label'      => __( 'Header Background Color', 'sju-theme' ),
            'section'    => 'colors',
            'settings'   => 'header_bg_color'
        )
    )
  );

	
	
	
	// Add the theme footer panel
	
	$wp_customize->add_panel( 'sjutheme_panel', array(
		'priority'       => 130,
		'capability'     => 'edit_theme_options',
		'theme_supports' => '',
		'title'          => esc_html__( 'Theme Options', 'sju-theme' ),
		'description'    => esc_html__( 'SJU Theme Options', 'sju-theme' ),
	));
	
	$wp_customize->add_section( 'sjutheme_footer_settings', array(
		'title'	=> esc_html__( 'Footer', 'sju-theme' ),
		'panel'	=> 'sjutheme_panel',
	));

  // Display site title in the footer checkbox.
  $wp_customize->add_setting( 'sjutheme_footer_branding', array(
    'default'           => '1',
    'sanitize_callback' => 'sjutheme_sanitize_checkbox',
  ));

  $wp_customize->add_control( 'sjutheme_footer_branding', array(
    'label'             => esc_html__( 'Show site title and description in the footer.', 'sju-theme' ),
    'section'           => 'sjutheme_footer_settings',
    'type'              => 'checkbox',
  ));

		$wp_customize->add_setting('sjutheme_footer_sidebar_bg', array(
			'transport'			=> 'refresh',
			'sanitize_callback' => 'esc_url_raw',
			)
		);

		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize,
			'sjutheme_footer_sidebar_bg',
			array(
				'label'		=> __( 'Footer Widget Area Background Image', 'sju-theme' ),
				'section'	=> 'sjutheme_footer_settings',
			)
		));

		$wp_customize->add_setting( 'sjutheme_footer_opacity', array(
			'default'           => '0.8',
			'sanitize_callback' => 'sjutheme_sanitize_opacity',
			'transport'         => 'refresh',
		) );

		$wp_customize->add_control( 'sjutheme_footer_opacity', array(
		'label'       => esc_html__( 'Footer Widget Area Background Opacity', 'sju-theme' ),
		'section'     => 'sjutheme_footer_settings',
		'type'        => 'select',
		'active_callback' => 'sjutheme_footer_background',
		'description' => esc_html( 'Set the opacity of the footer widget area overlay.', 'sju-theme' ),
		'choices'     => array(
			'0.2' => esc_html__( '20%', 'sju-theme' ),
			'0.4' => esc_html__( '40%', 'sju-theme' ),
			'0.6' => esc_html__( '60%', 'sju-theme' ),
			'0.8' => esc_html__( '80%', 'sju-theme' ),
			'1'   => esc_html__( '100%', 'sju-theme' ),
		),
		));
}
add_action( 'customize_register', 'sju_theme_customize_register' );



if ( ! function_exists( 'sjutheme_sanitize_checkbox' ) ) :
/**
 * Sanitize the checkbox.
 *
 * @param  mixed 	$input.
 * @return boolean	(true|false).
 */
function sjutheme_sanitize_checkbox( $input ) {
	if ( 1 == $input ) {
		return true;
	} else {
		return false;
	}
}
endif;

if ( ! function_exists( 'sjutheme_sanitize_opacity' ) ) :
/**
 * Sanitize the checkbox.
 *
 * @param  boolean	$input.
 * @return boolean	(true|false).
 */
function sjutheme_sanitize_opacity( $input ) {
	$choices = array( 0.2, 0.4, 0.6, 0.8, 1 );

	if ( ! in_array( $input, $choices ) ) {
		$input = 0.8;
	}

	return $input;
}
endif;

if ( ! function_exists( 'sjutheme_footer_background' ) ) :
/**
 * Active callback for canape_footer_opacity control.
 *
 * @param  object	$control.
 * @return boolean	(true|false).
 */
function sjutheme_footer_background( $control ) {
	if ( '' == $control->manager->get_setting( 'sjutheme_footer_sidebar_bg' )->value() ) {
		return false;
	} else {
		return true;
	}
}
endif;

/*
 * Output our custom CSS to change background colour/opacity of panels.
 * Note: not very pretty, but it works.
 */
function sjutheme_customizer_css( $control ) {
	// Adjust the opacity of featured image if set
	if ( get_theme_mod( 'sjutheme_footer_sidebar_bg' ) ) :
		if ( get_theme_mod( 'sjutheme_footer_opacity' ) ) :
		?>
			<style type="text/css">
			.pre-footer:after {
				opacity:  <?php echo esc_attr( get_theme_mod( 'sjutheme_footer_opacity' ) ); ?>;
			}
			.site-header { background-color: <?php echo get_theme_mod( 'header_bg_color' ); ?>; }
			</style>
		<?php
		endif;
	endif;
	
}
add_action( 'wp_head', 'sjutheme_customizer_css' );



/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function sju_theme_customize_preview_js() {
	wp_enqueue_script( 'sju_theme_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'sju_theme_customize_preview_js' );
