var gulp        = require('gulp');
var sass        = require('gulp-sass');
var browserSync = require('browser-sync').create();
var postcss      = require('gulp-postcss');
var sourcemaps   = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');
var filter       = require('gulp-filter');
var sourcemaps   = require('gulp-sourcemaps');
var flexibility  = require('postcss-flexibility');


///////////////////////////////////////////////////////////////////////////
//////////
//////////     WEBSITE TASKS
//////////
///////////////////////////////////////////////////////////////////////////

// JS

// Sass
gulp.task('sass', function() {
  var processors = [
    autoprefixer({browsers: ['last 4 versions']}),
    flexibility
  ];
  return gulp.src(['assets/sass/**/*.scss', '!assets/sass/vendor/**/*.scss'])
      .pipe(sourcemaps.init())
      .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
      .pipe(postcss(processors))
      .pipe(sourcemaps.write())
      .pipe(filter(noPartials))
      .pipe(gulp.dest('assets/css'))
      .pipe(browserSync.stream());
});


gulp.task('serve',['sass'], function() {
    browserSync.init({
        logPrefix: 'mission.dev',
        proxy: 'mission.dev'
    });

    gulp.watch("assets/sass/**/*.scss", ['sass']);
    gulp.watch("./*.*").on('change', browserSync.reload);
});


var noPartials = function (file) {
    var path         = require('path');
    var dirSeparator = path.sep.replace('\\', '\\\\');
    var relativePath = path.relative(process.cwd(), file.path);
    return !new RegExp('(^|'+dirSeparator+')_').test(relativePath);
};

///////////////////////////////////////////////////////////////////////////
//////////
//////////     WATCH AND BUILD TASKS
//////////
///////////////////////////////////////////////////////////////////////////

gulp.task('default', ['serve']);
