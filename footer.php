<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SJU_Wordpress_Theme
 */

?>

	</div><!-- #content -->

  <?php get_sidebar( 'footer' ); ?>
  
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
  		Saint Joseph's University <span class="sep"> | </span> 5600 City Ave <span class="sep"> | </span> Philadelphia, PA 19131
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
