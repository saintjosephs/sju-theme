<?php
  
  // TODO - allow this photo to be set on a per-page basis (maybe)
  // TODO - allow this photo to be sent thru the customizer on a site-wide basis
  // TODO - allow this to be a COLOR instead of a photo (site-wide)
  
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id( get_option('page_on_front') ), 'full' );
	$post_image = $thumb['0'];
?>


	<div class="hero" id="hero" style="background-image: url( '<?php echo esc_url( wp_get_attachment_image_src( get_post_thumbnail_id( get_option('page_on_front') ), 'full' )[0] ); ?>' );">
  	<div class="hero-middle">
      <div class="wrapper">
        <div class="site-branding">
    		  <?php
      		  if ( function_exists( 'jetpack_the_site_logo' ) && jetpack_has_site_logo() ) {
      				jetpack_the_site_logo();
      			}
      		?>
    			<?php
    			if ( is_front_page() && is_home() ) : ?>
    				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
    			<?php else : ?>
    				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
    			<?php
    			endif;
    
    			$description = get_bloginfo( 'description', 'display' );
    			if ( $description || is_customize_preview() ) : ?>
    				<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
    			<?php
    			endif; ?>
    		</div><!-- .site-branding -->
      </div>
    </div>
  	<div class="hero-overlay"></div>
	</div>
