<?php if(is_front_page()): ?>
  <?php get_template_part('template-parts/front', 'hero'); ?>
<?php else: ?>
  <?php get_template_part('template-parts/page', 'hero'); ?>
<?php endif; ?>

<div class="flex-wrapper main-wrapper">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">