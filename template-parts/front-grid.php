<?php

// check if the repeater field has rows of data
if( have_rows('grid_section_boxes') ):

?>

<div class="flex-grid full-width flex-quarters widget">

<?php
 	// loop through the rows of data
    while ( have_rows('grid_section_boxes') ) : the_row();
    ?><section class="grid-section"><?php
        // display a sub field value
        the_sub_field('grid_box');
    ?></section><?php
    endwhile;
?></div><?php

else :

    // no rows found

endif;

?>