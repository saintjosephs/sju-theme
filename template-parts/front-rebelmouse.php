<?php if(get_field('rebel_mouse_area')): ?>

<section class="rebel-feed wrapper">
  <h2><?php echo get_field('rebel_mouse_area_header');?></h2>
  <?php
		if ( has_nav_menu( 'social' ) ) {
			wp_nav_menu( array( 'theme_location' => 'social', 'depth' => 1, 'link_before' => '<span class="screen-reader-text">', 'link_after' => '</span>', 'container_class' => 'social-links' ) );
		}
	?>
  <iframe height="1500px" width="100%" scrolling="no" frameborder="0" src="<?php echo get_field('rebel_mouse_source_url');?>" class="rebelmouse-embed-iframe flexible" id="rebelmouse-embed-iframe" style="height: 1500px;"></iframe>
</section>

<?php endif; ?>