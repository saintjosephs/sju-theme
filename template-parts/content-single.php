<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SJU_Wordpress_Theme
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php 
		$categories_list = get_the_category_list( ' &middot; ' );
		if ( $categories_list ) {
			printf( '<span class="cat-links">%1$s</span>', $categories_list ); // WPCS: XSS OK.
		}; 
		?>

		<?php if ( 'link' == $format ) : ?>
			<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s">', esc_url( button_get_link_url() ) ), '</a></h1>' ); ?>
		<?php else : ?>
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<?php endif; ?>

		<div class="entry-meta">
			<?php //sjutheme_posted_on(); ?>
		</div>
	</header><!-- .entry-header -->
  
	<div class="entry-content">
		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'sju-theme' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'sju-theme' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php //sju_theme_entry_footer(); ?>
	</footer><!-- .entry-footer -->
	
</article><!-- #post-## -->
<?php if(!is_single()): ?>
<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/divider.png" class="entry-divider" alt="">
<?php endif; ?>
