<?php if(get_field('mission_statement')): ?>
<section class="mission-statement reverse">
  <div class="wrapper">
  <?php the_field('mission_statement'); ?>
  </div>
</section>
<?php endif; ?>