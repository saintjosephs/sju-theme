<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SJU_Wordpress_Theme
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>
<?php if(!is_front_page()): ?> <body <?php body_class('not-home'); ?>>
<?php else: ?> <body <?php body_class('home'); ?>>
<?php endif; ?>

<div id="page" class="site">
	<header id="masthead" class="site-header reverse" role="banner">
  	<div class="flex-wrapper">
    	<div class="sju-logo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sju_logo.svg" alt="Saint Joseph's University"></div>
    	
      
  		<nav id="site-navigation" class="main-navigation" role="navigation">
  			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'sju-theme' ); ?></button>
  			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
  		</nav><!-- #site-navigation -->
		
  		<?php
  				if ( has_nav_menu( 'social' ) ) {
  					wp_nav_menu( array( 'theme_location' => 'social', 'depth' => 1, 'link_before' => '<span class="screen-reader-text">', 'link_after' => '</span>', 'container_class' => 'social-links' ) );
  				}
  			?>
  	</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
