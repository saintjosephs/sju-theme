<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SJU_Wordpress_Theme
 */

get_header(); ?>


<?php get_template_part('template-parts/wrapper', 'top'); ?>

	<?php
	while ( have_posts() ) : the_post();
		get_template_part( 'template-parts/content', 'page' );
		
	endwhile; // End of the loop.
	?>

<?php get_template_part('template-parts/wrapper', 'botnoside'); ?>

<?php get_template_part('template-parts/front', 'mission' ); ?>
<?php get_template_part('template-parts/front', 'grid'); ?>
<?php get_template_part('template-parts/front', 'rebelmouse'); ?>

<?php get_footer(); ?>