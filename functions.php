<?php
/**
 * SJU Wordpress Theme functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package SJU_Wordpress_Theme
 */

if ( ! function_exists( 'sju_theme_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function sju_theme_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on SJU Wordpress Theme, use a find and replace
	 * to change 'sju-theme' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'sju-theme', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );
  
  /*
	 * Enable support for jetpack site logo.
	 */
  add_image_size( 'sjutheme-site-logo', '450', '450' );
	add_theme_support( 'site-logo', array( 'size' => 'sjutheme-site-logo' ) );
	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'sju-theme' ),
		'social'	=> esc_html__( 'Social Menu', 'sju-theme' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'sju_theme_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'sju_theme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function sju_theme_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'sju_theme_content_width', 640 );
}
add_action( 'after_setup_theme', 'sju_theme_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function sju_theme_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'sju-theme' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
  register_sidebar( array(
		'name'          => esc_html__( 'First Footer Widget Area', 'sju-theme' ),
		'id'            => 'sidebar-2',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Second Footer Widget Area', 'sju-theme' ),
		'id'            => 'sidebar-3',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'sju_theme_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function sju_theme_scripts() {
  
  wp_enqueue_style( 'sju-theme-main', get_template_directory_uri() . '/assets/css/style.css' );
	wp_enqueue_style( 'sju-theme-style', get_stylesheet_uri() );

  wp_enqueue_script( 'flexbox-polyfill', get_template_directory_uri() . '/assets/js/flexibility.js', array('jquery'), true );
	wp_enqueue_script( 'sju-theme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'sju-theme-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'sju_theme_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';



/*
  Add shortcode to put the grid from the homepage on an inner page
*/

add_shortcode('frontpage_grid', 'frontpage_grid_shortcode');

function frontpage_grid_shortcode($atts) {
  $front_page_id = get_option( 'page_on_front' );
  $output = "";
  $output .= '<div class="flex-grid full-width flex-quarters widget">';
  
    while ( have_rows('grid_section_boxes',$front_page_id) ) : the_row();
    $output .= '<section class="grid-section">';
    $output .= get_sub_field('grid_box');
    $output .= '</section>';
    endwhile;
  $output .= '</div>';
  return $output;
}

add_filter( 'get_the_archive_title', function ($title) {
    if ( is_category() ) {
      $title = single_cat_title( '', false );
    }
    return $title;
});




/* From Automaticc's "Button" theme */

if ( ! function_exists( 'sjutheme_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function sjutheme_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>';

	$byline = '<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>';

	$formats = get_theme_support( 'post-formats' );
	$format = get_post_format();
	$postformat = '';

	if ( $format && in_array( $format, $formats[0] ) ) {
		$postformat = sprintf( '<span class="post-format"><a href="%1$s" title="%2$s">%3$s</a></span>', // WPCS: XSS OK.
								esc_url( get_post_format_link( $format ) ),
								sprintf( esc_attr_x( 'All %1$s posts', 'post format archives link', 'sju-theme' ), get_post_format_string( $format ) ),
								esc_html( get_post_format_string( $format ) ) );
	}

	echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>' . $postformat; // WPCS: XSS OK.

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		comments_popup_link( esc_html__( 'Leave a comment', 'sju-theme' ), esc_html__( '1 Comment', 'button' ), esc_html__( '% Comments', 'sju-theme' ) );
		echo '</span>';
	}

	edit_post_link( esc_html__( 'Edit', 'sju-theme' ), '<span class="edit-link">', '</span>' );
}
endif;
